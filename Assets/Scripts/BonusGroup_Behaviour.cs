﻿using UnityEngine;
using System.Collections;

public class BonusGroup_Behaviour : MonoBehaviour
{
    public float    pointValue = 1;
    
    private GameController  _gameController;
    private ParticleSystem  _particleSystem;

	// Use this for initialization
	void Start ()
    {
        _gameController = FindObjectOfType<GameController>();
        _particleSystem = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update ()
    {	
	}

    public void OnPickedUp()
    {
        if (_gameController)
            _gameController.OnBonusPickedUp(pointValue);

        if (_particleSystem)
            _particleSystem.Play();

        DestroyObject(GetComponentInChildren<Bonus_Behaviour>().gameObject);
        if (_particleSystem)
            DestroyObject(this.gameObject, _particleSystem.startLifetime);
    }
}
