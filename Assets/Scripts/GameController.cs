﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public GUIText  scoreText;
    public GameObject[] spawnableArray;

    private float   _points;
    private long    _bonusCount;

	// Use this for initialization
	void Start ()
    {
        this._points = 0;
        UpdateGuiText();
        this._bonusCount = FindObjectsOfType<BonusGroup_Behaviour>().LongLength;
        if (_bonusCount <= 0)
        {
            SpawnBonus(new Vector3(Random.Range(-9, 9), 0.0f, Random.Range(-9, 9)));
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
	}

    public void OnBonusPickedUp(float pts)
    {
        this._bonusCount--;
        this._points += pts;
        UpdateGuiText();

        if (this._bonusCount <= 0)
        {
            this._bonusCount = 0;
            for (int i = 0; i < Random.Range(1, 4); i++)
                SpawnBonus(new Vector3(Random.Range(-9, 9), 0.0f, Random.Range(-9, 9)));
        }
    }

    public void SpawnBonus(Vector3 pos)
    {
        if (spawnableArray.Length <= 0)
            return;

        GameObject obj = spawnableArray[Random.Range(0, spawnableArray.Length - 1)];
        obj.transform.position = pos;

        GameObject.Instantiate(obj);

        this._bonusCount++;
    }

    private void UpdateGuiText ()
    {
        if (scoreText)
        {
            scoreText.text = "Score : " + this._points;
        }
    }
}
