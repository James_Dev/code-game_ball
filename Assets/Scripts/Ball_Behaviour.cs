﻿using UnityEngine;
using System.Collections;

public class Ball_Behaviour : MonoBehaviour
{
    public float        speed = 1;
    
    [System.NonSerialized]
    public float        cameraYRotation;

	// Use this for initialization
	void Start ()
    {
    }

    static public Vector3 RotateY(Vector3 v, float angle)
    {
        float sin = Mathf.Sin(angle);
        float cos = Mathf.Cos(angle);

        float tx = v.x;
        float tz = v.z;
        
        v.x = (cos * tx) + (sin * tz);
        v.z = (cos * tz) - (sin * tx);

        return (v);
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        float   moveHorizontal = Input.GetAxis("Horizontal");
        float   moveVertical = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(moveHorizontal, 0.0f, moveVertical);

        direction.Normalize();

        Vector3 movement = new Vector3(direction.x * speed, 0.0f, direction.z * speed);

        rigidbody.AddForce(RotateY(movement, cameraYRotation * Mathf.Deg2Rad));
	}
}
