﻿using UnityEngine;
using System.Collections;

public class BallCamera_Behaviour : MonoBehaviour
{
    public GameObject   ballObject;
    public float        distance;
    public float        height;
    public float        viewRotationSpeed;

    private Vector3     direction;
    private Ball_Behaviour  ballBehaviour;

	// Use this for initialization
	void Start()
	{
        direction = -ballObject.transform.forward;

        ballBehaviour = ballObject.GetComponent<Ball_Behaviour>();
        ballBehaviour.cameraYRotation = 0.0f;
	}
	
	// Update is called once per frame
	void Update()
	{
        if (Vector3.Distance(ballObject.transform.position, transform.position) > (-direction * distance).magnitude)
            transform.position = Vector3.Lerp(transform.position, ballObject.transform.position + (Vector3.up * height), 0.2f * Time.deltaTime);

        Transform tr = transform;
        tr.LookAt(ballObject.transform);

        transform.rotation = Quaternion.Lerp(transform.rotation, tr.rotation, 0.01f * Time.deltaTime);

        direction = Vector3.Lerp(direction, -ballObject.transform.forward, viewRotationSpeed);

        ballBehaviour.cameraYRotation = transform.rotation.eulerAngles.y;
	}
}
