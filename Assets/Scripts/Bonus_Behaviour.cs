﻿using UnityEngine;
using System.Collections;

public class Bonus_Behaviour : MonoBehaviour
{
    public float    rotationSpeed = 2.0f;

    private BonusGroup_Behaviour  _groupObject;

	// Use this for initialization
	void Start ()
    {
        GameObject objectParent = gameObject.transform.parent.gameObject;

        _groupObject = objectParent.GetComponent<BonusGroup_Behaviour>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	}

    // FixedUpdate is called
    void FixedUpdate ()
    {
        transform.Rotate(rotationSpeed, rotationSpeed, rotationSpeed);
    }

    void OnTriggerEnter (Collider other)
    {
        _groupObject.OnPickedUp();
    }
}
